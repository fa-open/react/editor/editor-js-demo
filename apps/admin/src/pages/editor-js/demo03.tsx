import React from 'react';
import { createReactEditorJS } from "react-editor-js";
import Header from '@editorjs/header'

const ReactEditorJS = createReactEditorJS();

/**
 * @author xu.pengfei
 * @date 2023/4/18 16:14
 */
export default function demo02() {
  return (
    <div>
      <h1 className="fa-text-center">Pure EditorJS + Header</h1>

      <ReactEditorJS
        /**
         * Common Inline Toolbar settings
         * - if true (or not specified), the order from 'tool' property will be used (default)
         * - if an array of tool names, this order will be used
         */
        // inlineToolbar={['link', 'marker', 'bold', 'italic']} // marker这个toolbar现行版本里好像没有，引入的话会导致报错
        inlineToolbar={['bold', 'italic', 'link']}
        tools={{
          header: {
            class: Header,
            /**
             * This property will override the common settings
             * That means that this tool will have only Marker and Link inline tools
             * If 'true', the common settings will be used.
             * If 'false' or omitted, the Inline Toolbar wont be shown
             */
            inlineToolbar: ['link'],
            config: {
              placeholder: 'Header'
            },
            shortcut: 'CMD+SHIFT+H'
          }
        }}
        defaultValue={{
          time: 1635603431943,
          blocks: [
            {
              id: "sheNwCUP5A",
              type: "header",
              data: {
                text: "Editor.js",
                level: 2
              }
            },
          ]
        }}
      />
    </div>
  )
}
