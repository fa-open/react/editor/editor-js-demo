import React, {useRef, useState} from 'react';
import {createReactEditorJS} from "react-editor-js";
import {Button, Space} from "antd";
import {EditorCore} from "@react-editor-js/core/src/editor-core";


const ReactEditorJS = createReactEditorJS();

/**
 * @author xu.pengfei
 * @date 2023/4/18 16:14
 */
export default function demo02() {
  const [editorCore, setEditorCore] = useState<EditorCore>()

  function handleSave() {
    console.log('editorCore,', editorCore)
    if (editorCore === undefined) return;

    editorCore.save().then((outputData:any) => {
      console.log('Article data: ', outputData)
    }).catch((error:any) => {
      console.log('Saving failed: ', error)
    });
  }

  return (
    <div className="fa-flex-column-center">
      <h1 className="fa-text-center">Pure EditorJS</h1>

      <Space>
        <Button onClick={handleSave}>保存</Button>
      </Space>

      <div style={{width: '100%'}}>
        <ReactEditorJS
          // ref={editorRef}
          // Pass autofocus option if you want to set a Caret to the Editor after initialization
          autofocus
          placeholder='Let`s write an awesome story!'
          // logLevel={LogLevels.ERROR} // 使用enum枚举会报错“为导出”，可能是包的问题
          // @ts-ignore
          logLevel="ERROR"
          readOnly={false}
          inlineToolbar={['bold', 'italic', 'link']}
          onInitialize={core => setEditorCore(core)}
          // onReady callback
          onReady={() => {
            console.log('Editor.js is ready to work!')
          }}
          onChange={(api, event) => {
            console.log('Now I know that Editor\'s content changed!', api, event)
          }}
        />
      </div>
    </div>
  )
}
