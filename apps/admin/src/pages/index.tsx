import React from 'react';
import { Link } from "react-router-dom";
import './index.scss'


/**
 * @author xu.pengfei
 * @date 2022/12/17 17:23
 */
export default function index() {

  return (
    <div className="fa-main">
      <div>
        <Link to="/editor-js/demo01">demo01</Link>
      </div>
    </div>
  );
}
